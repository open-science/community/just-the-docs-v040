<p align="right">
    <a href="https://badge.fury.io/rb/just-the-docs"><img src="https://badge.fury.io/rb/just-the-docs.svg" alt="Gem version"></a> <a href="https://github.com/just-the-docs/just-the-docs/actions/workflows/ci.yml"><img src="https://github.com/just-the-docs/just-the-docs/actions/workflows/ci.yml/badge.svg" alt="CI Build status"></a> <a href="https://app.netlify.com/sites/just-the-docs/deploys"><img src="https://api.netlify.com/api/v1/badges/9dc0386d-c2a4-4077-ad83-f02c33a6c0ca/deploy-status" alt="Netlify Status"></a>
</p>
<br><br>
<p align="center">
    <h1 align="center">Just the Docs</h1>
    <p align="center">A modern, highly customizable, and responsive Jekyll theme for documentation with built-in search.<br>Easily hosted on GitHub Pages with few dependencies.</p>
    <p align="center"><strong><a href="https://just-the-docs.github.io/just-the-docs/">See it in action!</a></strong></p>
    <br><br><br>
</p>

<p align="center">A video walkthrough of various Just the Docs features</p>

https://user-images.githubusercontent.com/85418632/211225192-7e5d1116-2f4f-4305-bb9b-437fe47df071.mp4


# just-the-docs -  WIN GitLab Pages theme

A simple and customisable theme for building your GitLab pages site.

## Usage

### 1. Fork this project

Fork this project by clicking the __*Fork*__ button at the top right corner of this page. Forking means that you now copied this entire project and all the files into your account.

### 2. Rename the project

In your version of the project, click on __*Settings*__ on the left (the cog icon) and rename the project (*Project name*) to something meaningful to you.

In the __*Advanced*__ settings (bottom of the page), check that the path matches what you expect given your desired project name.

### 3. Set the _config.yml url and update the "served at " description

In the file `_config.yml`, update line 18 so the baseurl matches the path of your project. The baseurl should start with `"/pages/"` and end without a trailing `/`.

In the project settings, update the description to match the address of where your build page will be rendered. This is the combined url+baseurl addresses from your `_config.yml`.

### 4. Turn on CI Runners

You need to enable a "runner" in order for GitLab to "build"/"serve" your page. Go to __*Settings*__ > __*CI/CD*__ > __*Runners*__. Tick `enable shared runners`.

### 5. Customize your website settings

Edit the `_config.yml` file to change any additional settings you want. To edit the file, click on it to view the file and then click on the pencil icon to edit it.  The settings in the file are self-explanatory and there are comments inside the file to help you understand what each setting does. Any line that begins with a hashtag (`#`) is a comment, and the other lines are actual settings.

**We suggest you start by making a small change (for example updating the title or author) then commit this change. Making the commit will trigger the CI, which will take a few moments to build your page.** You can monitor the progress of the CI in `CI/CD pipleines` (rocket icon on the left navigation bar).

### 6. Add your own content

To add pages to your site, you can either write a markdown file (`.md`) or you can write an HTML file. It's much easier to write markdown than HTML, so that's the recommended approach ([here's a great tutorial](https://markdowntutorial.com/) if you need to learn markdown in 5 minutes).

To see an example of a markdown file, click on any file that ends in `.md`, for example [`aboutme.md`](./aboutme.md). On that page you can see some nicely formatted text (there's a word in bold, a link, a few bullet points), and if you click on the pencil icon to edit the file, you'll see the markdown code that generated the pretty text. Very easy!

## Customisation

[View the documentation](https://pmarsceill.github.io/just-the-docs/) for additional usage usage information.

Take a look at the [Open WIN Community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/) to see an example of a customised version of this theme. Don't forget to look at the GitLab repository for that site (follow the "[view on GitLab link](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community)" to see how the customisation was achieved.


## Contributing

Bug reports, proposals of new features, and pull requests are welcome on GitHub at https://github.com/just-the-docs/just-the-docs. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

### Submitting code changes:

- Submit an [Issue](https://github.com/just-the-docs/just-the-docs/issues) that motivates the changes, using the appropriate template
- Discuss the proposed changes with other users and the maintainers
- Open a [Pull Request](https://github.com/just-the-docs/just-the-docs/pulls)
- Ensure all CI tests pass
- Provide instructions to check the effect of the changes
- Await code review

### Design and development principles of this theme:

1. As few dependencies as possible
2. No build script needed
3. First class mobile experience
4. Make the content shine

## Testing and development

To set up your environment to test this theme locally (without pushing it to GitLab): fork this repo, the run `bundle install` from the root directory on your terminal.

A modern [devcontainer configuration](https://code.visualstudio.com/docs/remote/containers) for VSCode is included.

Your theme is set up just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When this theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be included in the gem.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

[^2]: [It can take up to 10 minutes for changes to your site to publish after you push the changes to GitHub](https://docs.github.com/en/pages/setting-up-a-github-pages-site-with-jekyll/creating-a-github-pages-site-with-jekyll#creating-your-site).

[Jekyll]: https://jekyllrb.com
[Just the Docs Template]: https://just-the-docs.github.io/just-the-docs-template/
[Just the Docs]: https://just-the-docs.github.io/just-the-docs/
[Just the Docs repo]: https://github.com/just-the-docs/just-the-docs
[GitHub Pages]: https://pages.github.com/
[Template README]: https://github.com/just-the-docs/just-the-docs-template/blob/main/README.md
[GitHub Pages / Actions workflow]: https://github.blog/changelog/2022-07-27-github-pages-custom-github-actions-workflows-beta/
[use the template]: https://github.com/just-the-docs/just-the-docs-template/generate
